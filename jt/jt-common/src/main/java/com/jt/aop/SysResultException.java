package com.jt.aop;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.vo.SysResult;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice   //作用: 标识我是一个通知方法,并且只拦截Controll层的异常.并且返回JSON.
public class SysResultException {

    //需要定义一个全局的方法 返回指定的报错信息.
    //ExceptionHandler 配置异常的类型,当遇到了某种异常时在执行该方法.
    //JSONP的异常处理应该是 callback({status:201,msg:"",data:""})
    //利用Request对象动态获取callback参数.之后动态封装返回值
    @ExceptionHandler(RuntimeException.class)
    public Object exception(Exception e, HttpServletRequest request){
        e.printStackTrace();
        String callback = request.getParameter("callback");
        if(!StringUtils.isEmpty(callback)){ //jsonp请求
            return new JSONPObject(callback, SysResult.fail());
        }
           //日志记录/控制台输出. 让程序员知道哪里报错!!!
        return SysResult.fail();
    }
}
