package com.jt.quartz;

import java.util.Calendar;
import java.util.Date;

import com.jt.mapper.OrderMapper;
import com.jt.pojo.Order;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;


//准备订单定时任务
@Component
public class OrderQuartz extends QuartzJobBean{

	@Autowired
	private OrderMapper orderMapper;

	/**
	 * 如果用户30分钟之内没有完成支付,则将订单的状态status由1改为6.
	 * 条件判断的依据:  now()-创建时间 > 30分钟   <==>  created < now()-30
	 *
	 * sql: update tb_order set status=6,updated=#{updated} where status=1 and created< #{timeOut}
	 * @param context
	 * @throws JobExecutionException
	 */
	@Override
	@Transactional
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		//1.利用java工具API完成计算
		Calendar calendar = Calendar.getInstance();  //获取当前的时间
		calendar.add(Calendar.MINUTE,-30);
		Date timeOut = calendar.getTime();	//获取超时时间

		Order order = new Order();
		order.setStatus(6);
		//order.setUpdated(new Date());
		UpdateWrapper<Order> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("status", "1").lt("created",timeOut);
		orderMapper.update(order, updateWrapper);
		System.out.println("定时任务执行");
	}
}
