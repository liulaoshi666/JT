<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="/css/jt.css" />
<script type="text/javascript" src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
</head>
<body class="easyui-layout">

  <div data-options="region:'west',title:'菜单',split:true" style="width:10%;">
   		<ul class="easyui-tree">
   			<li>
   			<span>商品管理</span>
   			<ul>
   			<li><a onclick="addTab('商品新增','/item-add')">商品新增</a></li>
   			<li><a onclick="addTab('商品查询','/item-list')">商品查询</a></li>
   			<li><a onclick="addTab('商品更新','/item-update')">商品更新</a></li>
   			</ul>
			</li>
			
			<li>
   			<span>网站内容管理</span>
   			<ul>
   			<li>内容新增</li>
   			<li>内容修改</li>
   			</ul>
			</li>
   		
   		</ul>
   </div>
   <div id="tt" class="easyui-tabs" data-options="region:'center',title:'首页'"></div>


</body>
<script type="text/javascript">
//addTab('商品新增','/item-add')
function addTab(title, url){  
    if ($('#tt').tabs('exists', title)){
        $('#tt').tabs('select', title);
    } else {
        //iframe 画中画效果.
        var url2 = "https://map.baidu.com/poi/%E5%A4%A9%E5%AE%89%E9%97%A8/@12958171.00766985,4825984.883234514,18.45z?uid=65e1ee886c885190f60e77ff&ugc_type=3&ugc_ver=1&device_ratio=1&compat=1&querytype=detailConInfo&da_src=shareurl";
        var content = '<iframe scrolling="auto" frameborder="0"  src="'+url2+'" style="width:100%;height:100%;"></iframe>';
        $('#tt').tabs('add',{  
            title:title,  
            content:content,  
            closable:true  
        });  
    }
} 

</script>

</html>