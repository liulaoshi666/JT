package com.jt.web.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.ItemDesc;
import com.jt.util.ObjectMapperUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JSONPController {

    /**
     * 测试跨域访问是否成功
     * url地址: http://manage.jt.com/web/testJSONP?callback=jQuery111106536880527642785_1600138715329&_=1600138715330
     * 返回值的应该是经过特殊格式封装的数据.  callback(JSON)
     * JSONPObject参数说明:
     *  1.function 回调函数名称
     *  2.返回的对象之后可以被转化为JSON
     */
    @RequestMapping("/web/testJSONP")
    public JSONPObject jsonp(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("API测试!!!");
        return new JSONPObject(callback, itemDesc);
    }




   /* public String  jsonp(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("跨域测试成功!!!!");
        String json = ObjectMapperUtil.toJSON(itemDesc);
        return callback+"("+json+")";
    }*/
}
