package com.jt.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.jt.vo.EasyUITable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//1.标识配置类  配置类一般与@Bean注解联用!!!
@Configuration
public class MybatisPlusConfig {

    //需要将对象交给容器管理Map集合  map<paginationInterceptor方法名,实例化之后的对象>
    //Spring注入 1.按照类型注入   2.可以按照名字注入
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }
}
